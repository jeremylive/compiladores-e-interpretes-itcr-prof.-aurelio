import copy
from utils.arbol import TipoNodo


class NodoArbol:
    tipo: TipoNodo
    nodos: list
    contenido: str
    atributos: dict

    def __init__(self, tipo, contenido=None, nodos=None, atributos=None):

        if atributos is None:
            atributos = {}
        if nodos is None:
            nodos = []

        self.tipo = tipo
        self.contenido = contenido
        self.nodos = nodos
        self.atributos = copy.deepcopy(atributos)

    def visitar(self, visitante):
        return visitante.visitar(self)

    def __str__(self):

        # Coloca la información del nodo
        resultado = '{:30}\t\t'.format(self.tipo)

        if self.contenido is not None:
            resultado += '{:30}\t'.format(self.contenido)
        else:
            resultado += '{:30}\t'.format('')

        if self.atributos != {}:
            resultado += '{:38}\t'.format(str(self.atributos))
        else:
            resultado += '{:38}\t'.format('')

        if self.nodos:
            funciones_estandar = [
                ('los pigmentos son parecidos', TipoNodo.TipoNodo.ASIGNACION),
                ('La estructura viral', TipoNodo.TipoNodo.FUNCION),
                ('Aca esta la letra de doctor', TipoNodo.TipoNodo.IDENTIFICADOR),
                ('Se le necesita en urgencias ', TipoNodo.TipoNodo.LLAMADA_FUNCION),
                ('Una tableta de', TipoNodo.TipoNodo.PARAMETROS_FUNCION),
                ('Llamada urgente para', TipoNodo.TipoNodo.PARAMETROS_INVOCACION),
                ('Tapece la boca', TipoNodo.TipoNodo.SI_CONDICIONAL),
                ('Lavese las manos despues de esto', TipoNodo.TipoNodo.SINO_CONDICIONAL),
                ('a 2 metros de distancia para evitar contagio', TipoNodo.TipoNodo.NO_CONDICIONAL),
                ('El doctor recomienda', TipoNodo.TipoNodo.INSTRUCCION),
                ('El gobierno mantiene restricciones', TipoNodo.TipoNodo.REPETIR_WHILE),
                ('El gobierno tiene restriccion de placas', TipoNodo.TipoNodo.REPETIR_FOR),
                ('Se solicita al Doctor Logica a operaciones', TipoNodo.TipoNodo.OPERADOR_LOGICO),
                ('Formula de la medicina', TipoNodo.TipoNodo.EXPRESION_MATEMATICA),
                ('Llamada de emergencia', TipoNodo.TipoNodo.INVOCACION),
                ('Corte de paciente de grado', TipoNodo.TipoNodo.BIFURCACION),
                ('Mensaje urgente para', TipoNodo.TipoNodo.EXPRESION),
                ('El libro de medicina dice', TipoNodo.TipoNodo.CONDICION),
                ('Son los mismos padecimientos que', TipoNodo.TipoNodo.COMPARACION),
                ('Regurgitar', TipoNodo.TipoNodo.RETORNO),
                ('Se complico el paciente', TipoNodo.TipoNodo.ERROR),
                ('Dr Medico Cirujano', TipoNodo.TipoNodo.PRINCIPAL),
                ('Los documentos del paciente son', TipoNodo.TipoNodo.BLOQUE_INSTRUCCIONES),
                ('Necesitamos Operar de imnediato', TipoNodo.TipoNodo.OPERADOR),
                ('N95', TipoNodo.TipoNodo.MASCARILLA),
                ('Esta formula es igual que', TipoNodo.TipoNodo.COMPARADOR),
                ('tome cada 2 dias un poco de', TipoNodo.TipoNodo.RECETA),
                ('SARS-COV-2', TipoNodo.TipoNodo.MOLECULA),
                ('COV-2', TipoNodo.TipoNodo.MICROMOLECULA),
                ('', TipoNodo.TipoNodo.PALABRA_CLAVE),
                ('Dictamen medico', TipoNodo.TipoNodo.IMPRESION),
                ('tomese tambien un poco de', TipoNodo.TipoNodo.CONCATENAR),
                ('Cantidad de pastillas', TipoNodo.TipoNodo.INDICE),
                ('mililitros', TipoNodo.TipoNodo.LARGO),
                ('Consultar', TipoNodo.TipoNodo.SOLICITAR),
                ('En lugar de tomar esto puede tomar de', TipoNodo.TipoNodo.CONVERTIR)
            ]
            for nombre, tipo in funciones_estandar:
                if self.tipo == tipo:
                    resultado += '<'
                    resultado += '{}'.format(nombre)
                    resultado += '>'
                    break

        return resultado
