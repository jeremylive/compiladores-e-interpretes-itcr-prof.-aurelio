# Compiladores e interpretes ITCR Prof. Aurelio

# Colaboradores:
#### Jeremy, Luis, Juan y Alonso

### Contenido
#### Carpetas:
- analizador
- verificador
- generador
- explorador
- docs
- utils
    - arbol

#### Archivo Main:
- pandemico.py

#### Archivos en carpetas:

- Carpeta analizador:
    - analizadorArbol.py
    - analizadorGramatica.py

- Capeta verificador:
    - TablaSimbolos.py
    - verificador.py
    - Visitante.py

- Carpeta generador:
    - generador.py
    - plantillas.py
    - visitadores.py

- Carpeta explorador:
    - AtributoComponente.py
    - CategoriaComponente.py
    - ComponenteLexico.py
    - DescriptoresComponentes.py
    - ExploradorDeComponentes.py

- Carpeta docs:  
    - ebnf: ebnf.txt 
    - ejemplos: calcularimpuesto.covid, eliminarcaracter.covid, factorial.covid, factorialerror.covid, holamundo.covid, sumatoria.covid, Caracoles.covid
    - Proyecto4_Verificador.pdf
    - Proyecto5_Generador.pdf
    - Pandemico Video.mp4

- Carpeta utils:
    - archivo.py
    - tipos.py
    - arbol:
        - ArbolAbstracto.py
        - NodoArbol.py
        - TipoNodo.py

### Para pruebas:
Ingresar el nombre del archivo que se desea probar en el archivo `pandemico.py` y posteriormente ejecutar `python3 pandemico.py` en la terminal.
